﻿'Copyright © 2016 - 2017, Luke Wybar and contributors. All Rights Reserved.

Imports System.Drawing.Imaging
Imports System.IO
Imports System.Net
Imports System.Runtime.Serialization
Imports System.Text
Imports Microsoft.VisualBasic.CompilerServices
Imports System.IO.Compression
Imports System.IO.Compression.FileSystem
Imports System.Security.Policy

Public Class Launch

    Dim ClientID As String
    Dim selectedPackVersion as decimal
    Dim Login as Login = new Login
    Dim loginStatus As Boolean = False
    Dim Path As String = System.Windows.Forms.Application.UserAppDataPath
    Dim PackList As List(Of Pack)
    Dim UpdateFlag as boolean


    Private Sub BtnGo_Click(sender As Object, e As EventArgs) Handles BtnGo.Click
        If UserNameTxt.Text = "" Then
            MsgBox("Username/email empty, enter username/email")
            Exit Sub
        End If
        If PasswordTxt.Text = "" Then
            MsgBox("Password empty, enter Password")
            Exit Sub
        End If
        Login.UserName = Authenticate(UserNameTxt.Text, PasswordTxt.Text)
        If Login.UserName = "1" Then
            MsgBox("An error occured. Is your username and password correct?")
            Exit Sub
        End If
        WriteOut(1)
        'My.Computer.FileSystem.WriteAllText(Path & "\Profile.txt", SelectedUserName, 0)
        'My.Computer.FileSystem.WriteAllText(Path & "\accessToken.txt", accessToken, 0)
        'My.Computer.FileSystem.WriteAllText(Path & "\ProfileID.txt", UserID.ToString(), 0)
        IsLogin(False)

        'Refresh()
    End Sub
    Private Function WriteOut(Mode As Integer) As data
        Dim list = LoadIn(3)
        WriteOut = new data
        Dim WriteOutString as string
        WriteOutString = ""
        If Mode = 1 Then
            WriteOut.ID = "1"
            WriteOut.data = login.username & "," & Login.AccessToken & "," & Login.UserID.ToString()
            list.Add(WriteOut)
        End If
        If Mode = 2 Then
            WriteOut.ID = "2"
            WriteOut.Data = Guid.NewGuid().ToString()
            list.Add(WriteOut)
        End If
        If Mode = 3 Then
            list = RemoveCSV(1) 
        End If
        If mode = 4 then
            list = RemoveCSV(1)
            WriteOut.ID = "1"
            WriteOut.data = login.username & "," & Login.AccessToken & "," & Login.UserID.ToString()
            list.Add(WriteOut)
        End If
        
        For each Item As Data in list
            WriteOutString = Writeoutstring & Item.WriteOut() & vbcrlf
        Next

        My.Computer.FileSystem.WriteAllText(Path & "\Data.txt", WriteOutString, 0)
        
    End Function

    Private Function LoadIn(Mode As Integer) As Object
        Dim list as List(Of Data)
        list = New List(Of data)
        If My.Computer.FileSystem.FileExists(Path & "\Data.txt") = False
            File.Create(Path & "\Data.txt").Dispose()
        End If
        Dim reader as StreamReader = My.Computer.FileSystem.OpenTextFileReader(Path & "\Data.txt")
        Dim a as String
        
        Do
            Dim input as data
            input = New Data()
            
            a = reader.ReadLine
            MsgBox(Chr(34) & a & Chr(34))
            If a <> "" then
                input.ID = mid(a,1,1)
                MsgBox(Mid(a,3,Len(a ) - 1))
                input.Data = Mid(a,3,Len(a) - 1)
                list.Add(input)
            end if
        Loop Until reader.EndOfStream
        reader.Close()

        If Mode = 1 Then
            Dim LoginDetails() as string
            For each Input As data In list 
                If Input.ID = 1 Then
                    LoginDetails = Input.Data.Split(",")
                    Login.UserName = LoginDetails(0)
                    Login.AccessToken = LoginDetails(1)
                    Login.UserID = LoginDetails(2)
                    LoadIn = Input.data
                End If
            Next
        End If
        If Mode = 2 Then
            For each Input As data In list 
                If Input.ID = 2 Then
                    LoadIn = Input.data
                End If
            Next
        End If
        If Mode = 3 Then
            LoadIn = list
        End If
        If Mode = 4 then
            For each Input As data In list 
                If Input.ID = 1 Then
                    LoadIn = True
                End If
            Next
        End If

        Return(Loadin)



    End Function


    Private Function Authenticate(username As String, password As String) As Object
        Dim jsonData As Json = ConvertJson(username, password)
        Dim data = jsonData.Value
        Const authServer = "https://authserver.mojang.com/authenticate"
        Dim resultPost = SendRequest(authServer, data, "application/json", "POST", 0)
        If resultPost <> "1" Then
            'MsgBox("Yggdrasil server response:" & vbCrLf & resultPost)
            username = GetUsername(resultPost)
            Login.UserID = (GetUserID(resultPost))
            Login.AccessToken = GetAccessToken(resultPost)
            DisplaySkin(username)
            loginStatus = True
            Return (username)
        Else
            loginStatus = False
            Return "1"
        End If
    End Function

    Private Sub Refresh()
        Dim jsonData As Json = RefreshJson()
        Dim data = jsonData.Value
        Const authServer = "https://authserver.mojang.com/refresh"
        Dim resultPost = SendRequest(authServer, data, "application/json", "POST",1)
        If resultPost <> "1" Then
            'MsgBox("Yggdrasil server response:" & vbCrLf & resultPost)
            Login.AccessToken = GetAccessToken(resultPost)
            loginStatus = True
        Else
            loginStatus = False
        End If
        DisplaySkin(Login.UserName)
    End Sub

    Private Function RefreshJson() As Json
        RefreshJson = New Json()
        RefreshJson.Value = RefreshJson.Value & "{"
        RefreshJson.Value = RefreshJson.Value & Chr(34) & "accessToken" & Chr(34) & ": " & Chr(34) & Login.AccessToken & Chr(34) & ","
        RefreshJson.Value = RefreshJson.Value & Chr(34) & "clientToken" & Chr(34) & ": " & Chr(34) & ClientID & Chr(34) & ","
        RefreshJson.Value = RefreshJson.Value & Chr(34) & "requestUser" & Chr(34) & ": false"
        RefreshJson.Value = RefreshJson.Value & "}"
        'MsgBox(RefreshJson.Value)
        Return RefreshJson
    End Function

    Private Sub Launch_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        
        MsgBox(String.Format("TotalPhysicalMemory: {0} MBytes", System.Math.Round(My.Computer.Info.TotalPhysicalMemory / (1024 * 1024)) - 2048, 2).ToString)
        Dim clientGuid As Guid
        PackList = New List(Of Pack)
        Dim LastMark As Integer
        Dim Counts As Integer
        For counter = 1 To Len(Path)
            If Mid(Path, counter, 1) = "\" Then
                LastMark = counter
                Counts = Counts + 1
            End If
            If Counts = 5 Then
                Exit For
            End If
        Next
        'MsgBox(LastMark)
        Path = Mid(Path, 1, LastMark) & "WybarLauncher"
        Try
            clientGuid = Guid.Parse(LoadIn(2))
        Catch ex As Exception
            WriteOut(2)
        End Try
        ClientID = clientGuid.ToString("N")

        If LoadIn(4) = True Then
            loginStatus = True
            IsLogin(True)
        Else
            loginStatus = False
        End If
        Try
            Dim Client As New WebClient
            Client.DownloadFile("http://wybar.uk/Launcher/packs.txt", Path & "\packs.txt")
            If Not File.Exists(Path & "\AWS.zip") Then
                MsgBox("Downloading prerequisites, don't panic this may take some time!")
                Client.DownloadFile("http://wybar.uk/er/AWS.zip", Path & "\AWS.zip")
                Client.Dispose()
                Extract(Path & "\AWS.zip", Path & "\AWS\")
            End If


            Client.Dispose()
        Catch ex As Exception

        End Try
        'MsgBox(Path)

        LoadList()
        'MsgBox(Path)
    End Sub

    Private Sub LoadList()
        Dim Pack As New Pack
        Using ioReader As New Microsoft.VisualBasic.FileIO.TextFieldParser(Path & "\packs.txt")

            ioReader.TextFieldType = FileIO.FieldType.Delimited
            ioReader.SetDelimiters(",")

            While Not ioReader.EndOfData

                Dim arrCurrentRow As String() = ioReader.ReadFields()

                Pack.Name = arrCurrentRow(0)
                Pack.Version = arrCurrentRow(1)
                Pack.ID = arrCurrentRow(2)
                PackList.Add(Pack)
                PacksList.Items.Add(Pack)
            End While

        End Using
    End Sub

    Private Sub IsLogin(StartupFlag As Boolean)
        UserNameTxt.Visible = False
        PasswordTxt.Visible = False
        Label1.Visible = False
        Label2.Visible = False
        BtnGo.Visible = False
        Label4.Visible = True
        PacksList.Visible = True
        LoggedIn.Visible = True
        LogOutBtn.Visible = True
        If StartupFlag = True Then
            LoadIn(1)
            'Login.AccessToken = My.Computer.FileSystem.ReadAllText(Path & "\accessToken.txt")
            'Login.UserName = My.Computer.FileSystem.ReadAllText(Path & "\Profile.txt")
            'Login.UserID = (My.Computer.FileSystem.ReadAllText(Path & "\ProfileID.txt"))
            Refresh()
        End If
        SelectedUserTxt.Text = "Selected user is: " & Login.UserName
    End Sub

    Private Function ConvertJson(username As String, password As String) As Json
        ConvertJson = New Json()
        ConvertJson.Value = ConvertJson.Value & "{"
        ConvertJson.Value = ConvertJson.Value & Chr(34) & "agent" & Chr(34) & ": {"
        ConvertJson.Value = ConvertJson.Value & Chr(34) & "name" & Chr(34) & ": " & Chr(34) & "Minecraft" & Chr(34) & ","
        ConvertJson.Value = ConvertJson.Value & Chr(34) & "version" & Chr(34) & ": 1"
        ConvertJson.Value = ConvertJson.Value & "},"
        ConvertJson.Value = ConvertJson.Value & Chr(34) & "username" & Chr(34) & ": " & Chr(34) & username & Chr(34) & ","
        ConvertJson.Value = ConvertJson.Value & Chr(34) & "password" & Chr(34) & ": " & Chr(34) & password & Chr(34) & ","
        ConvertJson.Value = ConvertJson.Value & Chr(34) & "clientToken" & Chr(34) & ": " & Chr(34) & ClientID & Chr(34) & ","
        ConvertJson.Value = ConvertJson.Value & Chr(34) & "requestUser" & Chr(34) & ": true"
        ConvertJson.Value = ConvertJson.Value & "}"
        'MsgBox(ConvertJson.Value)
        Return ConvertJson
    End Function

    Private Function SendRequest(uri As String, jsonData As String, contentType As String, method As String, Refresh As Integer) As String
            ' Create a request using a URL that can receive a post.   
            Dim request As WebRequest = WebRequest.Create(uri)
            ' Set the Method property of the request to POST.  
            request.Method = method
            ' Create POST data and convert it to a byte array.  
            Dim postData As String = jsonData
            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(postData)
            ' Set the ContentType property of the WebRequest.  
            request.ContentType = contentType
            ' Set the ContentLength property of the WebRequest.  
            request.ContentLength = byteArray.Length
            ' Get the request stream.  
            Dim dataStream As Stream = request.GetRequestStream()
            ' Write the data to the request stream.  
            dataStream.Write(byteArray, 0, byteArray.Length)
            ' Close the Stream object.  
            dataStream.Close()
            MsgBox(Uri)
            MsgBox(jsonData)
            ' Get the response.  
            Try
            Dim response As Object = request.GetResponse()
            ' Display the status.  
            Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
            ' Get the stream containing content returned by the server.  
            dataStream = response.GetResponseStream()
            ' Open the stream using a StreamReader for easy access.  
            Dim reader As New StreamReader(dataStream)
            ' Read the content.  
            Dim responseFromServer As String = reader.ReadToEnd()
            ' Display the content.  
            ' Clean up the streams.  
            reader.Close()
            dataStream.Close()
            response.Close()
            Return (responseFromServer)
        Catch e As Exception
            If Refresh = 1 Then
                InvalidateToken()
                Logout()
                Return(-1)
            end if
            Return "1"
        End Try
    End Function
    Private Function GetUsername(Response As String) As String
        Dim Userend As Integer
        For counter = 167 To Len(Response)
            If Mid(Response, counter, 1) = Chr(34) Then
                Userend = counter
                Exit For
            End If
        Next
        Userend = Userend - 167
        'MsgBox(Mid(Response, 167, Userend))
        Return (Mid(Response, 167, Userend))
    End Function

    Private Function GetUserID(Response As String) As String
        Dim Userend As Integer
        For counter = 125 To Len(Response)
            If Mid(Response, counter, 1) = Chr(34) Then
                Userend = counter
                Exit For
            End If
        Next
        Userend = Userend - 125
        'MsgBox(Mid(Response, 125, Userend))
        Return (Mid(Response, 125, Userend))
    End Function

    Private Function GetAccessToken(Response As String) As String
        Dim Userend As Integer
        For counter = 17 To Len(Response)
            If Mid(Response, counter, 1) = Chr(34) Then
                Userend = counter
                Exit For
            End If
        Next
        Userend = Userend - 17

        'MsgBox(Mid(Response, 17, Userend))
        Return (Mid(Response, 17, Userend))
    End Function
    Private Sub DisplaySkin(Username As String)
        SkinBox.Visible = true
        Dim uril as Uri = new Uri("https://crafatar.com/renders/body/" & Login.UserID)

        SkinBox.url = uril

        'SkinBox.Image = Image.FromFile(Path & "\skins\" & Username & ".png")
    End Sub

    Private Sub LogOutBtn_Click(sender As Object, e As EventArgs) Handles LogOutBtn.Click
        Logout()
    End Sub

    Private sub Logout()
        WriteOut(3)
        Label6.Visible = False
        RunBTN.Visible = False
        UserNameTxt.Visible = True
        PasswordTxt.Visible = True
        Label1.Visible = True
        Label2.Visible = True
        BtnGo.Visible = True
        Label4.Visible = False
        PacksList.Visible = False
        PacksList.SelectedItem = Nothing
        LoggedIn.Visible = False
        LogOutBtn.Visible = False
        Label5.Visible = False
        InstallBTN.Visible = False
        InvalidateToken()
        SelectedUserTxt.Text = "No User Selected"
        SkinBox.visible = False
    End sub

    Private Sub InvalidateToken()
        Dim jsonData As Json = InvalidateJson()
        Dim data = jsonData.Value
        Const authServer = "https://authserver.mojang.com/invalidate"
        Dim resultPost = SendRequest(authServer, data, "application/json", "POST",0)
    End Sub

    Private Function InvalidateJson() As Json
        InvalidateJson = New Json
        InvalidateJson.Value = InvalidateJson.Value & "{"
        InvalidateJson.Value = InvalidateJson.Value & Chr(34) & "accessToken" & Chr(34) & ": " & Chr(34) & Login.AccessToken & Chr(34) & ","
        InvalidateJson.Value = InvalidateJson.Value & Chr(34) & "clientToken" & Chr(34) & ": " & Chr(34) & ClientID & Chr(34)
        InvalidateJson.Value = InvalidateJson.Value & "}"
        'MsgBox(InvalidateJson.Value)
        'MsgBox("Hi2")
        Return InvalidateJson
    End Function

    Private Sub PacksList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles PacksList.SelectedIndexChanged
        If PacksList.SelectedItem.ToString() = "" Then
            exit sub
        End If
        selectedPackVersion = PacksList.SelectedItem.Version 
        CheckInstall()
    End Sub

    Private Sub InstallBTN_Click(sender As Object, e As EventArgs) Handles InstallBTN.Click
        MsgBox("Please wait, this may take some time to complete.")
        Dim Client As New WebClient
        Dim URL As String = "http://wybar.uk/Launcher/packs/" & (PacksList.SelectedItem).ID & ".zip"
        If Not My.Computer.FileSystem.DirectoryExists(Path & "\modpacks\") Then
            MkDir(Path & "\modpacks\")
        End If
        Dim Uri1 As Uri
        Uri1 = New Uri(URL)
        'MsgBox(URL)
        Client.DownloadFile(Uri1, Path & "\modpacks\" & (PacksList.SelectedItem).ID & ".zip")
        DownloadProgress.Visible = True
        Label7.Visible = True
        Client.Dispose()
        Install()

    End Sub

    Private Sub client_ProgressChanged(ByVal sender As Object, ByVal e As DownloadProgressChangedEventArgs)
        Dim bytesIn As Double = Double.Parse(e.BytesReceived.ToString())
        DownloadProgress.Visible = True
        Dim totalBytes As Double = Double.Parse(e.TotalBytesToReceive.ToString())
        Dim percentage As Double = bytesIn / totalBytes * 100
        DownloadProgress.Value = Int32.Parse(Math.Truncate(percentage).ToString())
    End Sub

    Private Sub client_DownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        

    End Sub

    Private Sub Install()
        If UpdateFlag = false
            DownloadProgress.Visible = False
            Label7.Visible = False
            Extract(Path & "\modpacks\" & (PacksList.SelectedItem).ID & ".zip", (Path & "\modpacks\" & (PacksList.SelectedItem).ID) & "\")
            MsgBox("Download Complete")
            DownloadProgress.Visible = False
            Label7.Visible = False
            CheckInstall()
        Else
            DownloadProgress.Visible = False
            Label7.Visible = False
            updatePack()
            CheckInstall()
        End If
    End Sub


    private Sub updatePack()
        Extract(Path & "\modpacks\" & (PacksList.SelectedItem).ID & ".zip", (Path & "\modpacks\" & (PacksList.SelectedItem).ID) & "-update\")
        System.IO.Directory.Delete(Path & "\modpacks\" & ((PacksList.SelectedItem).ID) & "\Instances\VanillaMinecraft\mods\", true)

        MkDir(Path & "\modpacks\" & ((PacksList.SelectedItem).ID) & "\Instances\VanillaMinecraft\mods\")
        my.Computer.FileSystem.CopyDirectory(Path & "\modpacks\" & ((PacksList.SelectedItem).ID) & "-update\Instances\VanillaMinecraft\mods\", Path & "\modpacks\" & ((PacksList.SelectedItem).ID) & "\Instances\VanillaMinecraft\mods\")
        System.IO.Directory.Delete(Path & "\modpacks\" & ((PacksList.SelectedItem).ID) & "-update\", true)
        My.Computer.FileSystem.WriteAllText(Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\version.txt", selectedPackVersion, False)
        Kill(Path & "\modpacks\" & (PacksList.SelectedItem).ID & ".zip")
        MsgBox("Update Complete")
        DownloadProgress.Visible = False
        Label7.Visible = False

    End Sub

    Private Sub CheckInstall()
        Label5.Visible = True
        If My.Computer.FileSystem.DirectoryExists(Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\") Then
            If My.Computer.FileSystem.FileExists(Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\version.txt")
                dim InstalledVersion as decimal = My.Computer.FileSystem.ReadAllText(Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\version.txt")
                If  InstalledVersion>= selectedPackVersion
                    Label5.Text = "Modpack installed! :D"
                    Label6.Visible = True
                    RunBTN.Visible = True
                    InstallBTN.Visible = False
                    UpdateFlag = False
                Else 
                    Label5.Text = "Update Available!"
                    Label6.Visible = True
                    RunBTN.Visible = True
                    InstallBTN.Visible = True
                    InstallBTN.Text = "Update?"
                    UpdateFlag = True
                End If
            Else
                Label5.Text = "Update Available!"
                Label6.Visible = True
                RunBTN.Visible = True
                InstallBTN.Visible = True
                InstallBTN.Text = "Update?"
                UpdateFlag = True
            End If
        Else
            Label5.Text = "Modpack not installed :("
            Label6.Visible = False
            RunBTN.Visible = False
            InstallBTN.Visible = True
            UpdateFlag = False
        End If

    End Sub
    Private Sub Extract(InputPath As String, OutputPath As String)
        ZipFile.ExtractToDirectory(InputPath, OutputPath)
    End Sub

    Private Sub RunBTN_Click(sender As Object, e As EventArgs) Handles RunBTN.Click
        'Process.Start("javaw", "-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -XX:-OmitStackTraceInFastThrow -Xms256M -Xmx6144M -XX:MetaspaceSize=1024M -Duser.language=en -Duser.country=US -Dfml.ignorePatchDiscrepancies=true -Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.log.level=INFO -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy " & Chr(34) & "-Djava.library.path=C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\natives" & Chr(34) & " -cp " & Chr(34) & "C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\jarmods\forge-1.7.10-10.13.4.1614-1.7.10-universal.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\launchwrapper-1.12.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\asm-all-5.0.3.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\akka-actor_2.11-2.3.3.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\config-1.2.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\scala-actors-migration_2.11-1.1.0.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\scala-compiler-2.11.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\scala-continuations-library_2.11-1.0.2.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\scala-continuations-plugin_2.11.1-1.0.2.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\scala-library-2.11.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\scala-parser-combinators_2.11-1.0.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\scala-reflect-2.11.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\scala-swing_2.11-1.0.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\scala-xml_2.11-1.0.2.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\lzma-0.0.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\jopt-simple-4.5.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\guava-17.0.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\commons-lang3-3.3.2.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\netty-1.6.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\realms-1.3.5.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\commons-compress-1.8.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\httpclient-4.3.3.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\commons-logging-1.1.3.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\httpcore-4.3.2.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\vecmath-1.3.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\trove4j-3.0.3.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\icu4j-core-mojang-51.2.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\codecjorbis-20101023.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\codecwav-20101023.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\libraryjavasound-20101123.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\librarylwjglopenal-20100824.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\soundsystem-20120107.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\netty-all-4.0.10.Final.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\commons-io-2.4.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\commons-codec-1.9.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\jinput-2.0.5.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\jutils-1.0.0.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\gson-2.2.4.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\authlib-1.5.21.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\log4j-api-2.0-beta9.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\log4j-core-2.0-beta9.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\lwjgl-2.9.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\lwjgl_util-2.9.1.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\twitch-5.16.jar;C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft\bin\minecraft.jar" & Chr(34) & " net.minecraft.launchwrapper.Launch --username andrew159159 --version 1.7.10 --gameDir " & Chr(34) & "C:\Users\Luke Wybar\Desktop\ATLauncher\Instances\VanillaMinecraft" & Chr(34) & " --assetsDir " & Chr(34) & "C:\Users\Luke Wybar\Desktop\ATLauncher\Configs\Resources" & Chr(34) & " --assetIndex 1.7.10 --uuid ffadaa1d7bb6494b9cff651b1f1157b8 --accessToken " & accessToken & " --userProperties {} --userType mojang --width=854 --height=480 --tweakClass=cpw.mods.fml.common.launcher.FMLTweaker")
        'Process.Start("javaw", "-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -XX:-OmitStackTraceInFastThrow -Xms256M -Xmx6144M -XX:MetaspaceSize=1024M -Duser.language=en -Duser.country=US -Dfml.ignorePatchDiscrepancies=true -Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.log.level=INFO -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy " & Chr(34) & "-Djava.library.path=" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\natives" & Chr(34) & " -cp " & Chr(34) & "C:\Users\Luke Wybar\Desktop\ATLauncher\ATLauncher.exe;anything;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\jarmods\forge-1.7.10-10.13.4.1614-1.7.10-universal.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\launchwrapper-1.12.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\asm-all-5.0.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\akka-actor_2.11-2.3.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\config-1.2.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\scala-actors-migration_2.11-1.1.0.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\scala-compiler-2.11.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\scala-continuations-library_2.11-1.0.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\scala-continuations-plugin_2.11.1-1.0.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\scala-library-2.11.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\scala-parser-combinators_2.11-1.0.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\scala-reflect-2.11.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\scala-swing_2.11-1.0.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\scala-xml_2.11-1.0.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\lzma-0.0.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\jopt-simple-4.5.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\guava-17.0.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\commons-lang3-3.3.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\netty-1.6.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\realms-1.3.5.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\commons-compress-1.8.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\httpclient-4.3.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\commons-logging-1.1.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\httpcore-4.3.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\vecmath-1.3.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\trove4j-3.0.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\icu4j-core-mojang-51.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\codecjorbis-20101023.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\codecwav-20101023.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\libraryjavasound-20101123.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\librarylwjglopenal-20100824.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\soundsystem-20120107.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\netty-all-4.0.10.Final.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\commons-io-2.4.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\commons-codec-1.9.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\jinput-2.0.5.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\jutils-1.0.0.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\gson-2.2.4.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\authlib-1.5.21.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\log4j-api-2.0-beta9.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\log4j-core-2.0-beta9.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\lwjgl-2.9.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\lwjgl_util-2.9.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\twitch-5.16.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\bin\minecraft.jar" & Chr(34) & " net.minecraft.launchwrapper.Launch --username & SelectedUserName & --version 1.7.10 --gameDir " & Chr(34) & "" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "" & Chr(34) & " --assetsDir " & Chr(34) & "" & Path & "\modpacks\Assets\Configs" & "Resources" & Chr(34) & " --assetIndex 1.7.10 --uuid & UserID & --accessToken " & accessToken & " --userProperties {} --userType mojang --width=854 --height=480 --tweakClass=cpw.mods.fml.common.launcher.FMLTweaker")
        'MsgBox("-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -XX:-OmitStackTraceInFastThrow -Xms256M -Xmx" & System.Math.Truncate(System.Math.Round((My.Computer.Info.TotalPhysicalMemory / (1024 * 1024)) - 4096, 2)).ToString() & "M" & " -XX:MetaspaceSize=1024M -Duser.language=en -Duser.country=US -Dfml.ignorePatchDiscrepancies=true -Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.log.level=INFO -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy " & Chr(34) & "-Djava.library.path=" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\natives" & Chr(34) & " -cp " & Chr(34) & "" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\ATLauncher.exe;anything;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\jarmods\forge-1.7.10-10.13.4.1614-1.7.10-universal.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\launchwrapper-1.12.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\asm-all-5.0.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\akka-actor_2.11-2.3.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\config-1.2.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-actors-migration_2.11-1.1.0.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-compiler-2.11.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-continuations-library_2.11-1.0.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-continuations-plugin_2.11.1-1.0.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-library-2.11.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-parser-combinators_2.11-1.0.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-reflect-2.11.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-swing_2.11-1.0.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-xml_2.11-1.0.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\lzma-0.0.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\jopt-simple-4.5.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\guava-17.0.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-lang3-3.3.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\netty-1.6.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\realms-1.3.5.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-compress-1.8.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\httpclient-4.3.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-logging-1.1.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\httpcore-4.3.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\vecmath-1.3.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\trove4j-3.0.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\icu4j-core-mojang-51.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\codecjorbis-20101023.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\codecwav-20101023.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\libraryjavasound-20101123.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\librarylwjglopenal-20100824.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\soundsystem-20120107.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\netty-all-4.0.10.Final.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-io-2.4.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-codec-1.9.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\jinput-2.0.5.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\jutils-1.0.0.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\gson-2.2.4.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\authlib-1.5.21.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\log4j-api-2.0-beta9.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\log4j-core-2.0-beta9.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\lwjgl-2.9.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\lwjgl_util-2.9.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\twitch-5.16.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\minecraft.jar" & Chr(34) & " net.minecraft.launchwrapper.Launch --username " & SelectedUserName & " --version 1.7.10 --gameDir " & Chr(34) & "" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft" & Chr(34) & " --assetsDir " & Chr(34) & "" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Configs\Resources" & Chr(34) & " --assetIndex 1.7.10 --uuid " & UserID & " --accessToken " & accessToken & " --userProperties {} --userType mojang --width=854 --height=480 --tweakClass=cpw.mods.fml.common.launcher.FMLTweaker")
        Process.Start("Javaw", "-XX:HeapDumpPath=MojangTricksIntelDriversForPerformance_javaw.exe_minecraft.exe.heapdump -XX:-OmitStackTraceInFastThrow -Xms256M -Xmx" & System.Math.Truncate(System.Math.Round((My.Computer.Info.TotalPhysicalMemory / (1024 * 1024)) - 4096, 2)).ToString() & "M" & " -XX:MetaspaceSize=1024M -Duser.language=en -Duser.country=US -Dfml.ignorePatchDiscrepancies=true -Dfml.ignoreInvalidMinecraftCertificates=true -Dfml.log.level=INFO -XX:+UseConcMarkSweepGC -XX:+CMSIncrementalMode -XX:-UseAdaptiveSizePolicy " & Chr(34) & "-Djava.library.path=" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\natives" & Chr(34) & " -cp " & Chr(34) & "" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\ATLauncher.exe;anything;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\jarmods\forge-1.7.10-10.13.4.1614-1.7.10-universal.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\launchwrapper-1.12.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\asm-all-5.0.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\akka-actor_2.11-2.3.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\config-1.2.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-actors-migration_2.11-1.1.0.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-compiler-2.11.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-continuations-library_2.11-1.0.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-continuations-plugin_2.11.1-1.0.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-library-2.11.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-parser-combinators_2.11-1.0.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-reflect-2.11.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-swing_2.11-1.0.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\scala-xml_2.11-1.0.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\lzma-0.0.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\jopt-simple-4.5.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\guava-17.0.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-lang3-3.3.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\netty-1.6.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\realms-1.3.5.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-compress-1.8.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\httpclient-4.3.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-logging-1.1.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\httpcore-4.3.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\vecmath-1.3.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\trove4j-3.0.3.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\icu4j-core-mojang-51.2.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\codecjorbis-20101023.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\codecwav-20101023.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\libraryjavasound-20101123.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\librarylwjglopenal-20100824.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\soundsystem-20120107.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\netty-all-4.0.10.Final.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-io-2.4.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\commons-codec-1.9.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\jinput-2.0.5.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\jutils-1.0.0.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\gson-2.2.4.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\authlib-1.5.21.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\log4j-api-2.0-beta9.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\log4j-core-2.0-beta9.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\lwjgl-2.9.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\lwjgl_util-2.9.1.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\twitch-5.16.jar;" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft\bin\minecraft.jar" & Chr(34) & " net.minecraft.launchwrapper.Launch --username " & Login.UserName & " --version 1.7.10 --gameDir " & Chr(34) & "" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Instances\VanillaMinecraft" & Chr(34) & " --assetsDir " & Chr(34) & "" & Path & "\modpacks\" & (PacksList.SelectedItem).ID & "\Configs\Resources" & Chr(34) & " --assetIndex 1.7.10 --uuid " & Login.UserID & " --accessToken " & Login.AccessToken & " --userProperties {} --userType mojang --width=854 --height=480 --tweakClass=cpw.mods.fml.common.launcher.FMLTweaker")
        'C:\Users\Luke Wybar\Desktop\ATLauncher\ATLauncher.exe;anything;
        Application.Exit()
    End Sub

    Private Sub SubmitErrorBtn_Click(sender As Object, e As EventArgs) Handles SubmitErrorBtn.Click
        Dim webAddress As String = "http://redmine.wybar.uk/projects"
        Process.Start(webAddress)
    End Sub

    Private Sub SettingsBtn_Click(sender As Object, e As EventArgs) Handles SettingsBtn.Click
        Settings.Show()
        me.Hide()
    End Sub

    Private Function RemoveCSV(ID As Integer) As List(Of data)
        RemoveCSV = new List(Of Data)
        Dim list As List(Of data) = New List(Of data)
        list = LoadIn(3)
        For each Item As Data in list
            If Item.ID <> ID Then
                RemoveCSV.Add(Item)
            End If
        Next
        Return(RemoveCSV)
    End Function


End Class