﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Launch
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Launch))
        Me.UserNameTxt = New System.Windows.Forms.TextBox()
        Me.PasswordTxt = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.BtnGo = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SettingsBtn = New System.Windows.Forms.Button()
        Me.SkinBox = New System.Windows.Forms.WebBrowser()
        Me.SubmitErrorBtn = New System.Windows.Forms.Button()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.DownloadProgress = New System.Windows.Forms.ProgressBar()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.RunBTN = New System.Windows.Forms.Button()
        Me.InstallBTN = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.PacksList = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.LogOutBtn = New System.Windows.Forms.Button()
        Me.LoggedIn = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SelectedUserTxt = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout
        Me.SuspendLayout
        '
        'UserNameTxt
        '
        Me.UserNameTxt.Location = New System.Drawing.Point(362, 110)
        Me.UserNameTxt.Margin = New System.Windows.Forms.Padding(4)
        Me.UserNameTxt.Name = "UserNameTxt"
        Me.UserNameTxt.Size = New System.Drawing.Size(382, 31)
        Me.UserNameTxt.TabIndex = 0
        '
        'PasswordTxt
        '
        Me.PasswordTxt.Location = New System.Drawing.Point(362, 150)
        Me.PasswordTxt.Margin = New System.Windows.Forms.Padding(4)
        Me.PasswordTxt.Name = "PasswordTxt"
        Me.PasswordTxt.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasswordTxt.Size = New System.Drawing.Size(382, 31)
        Me.PasswordTxt.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(190, 113)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 25)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "User"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(144, 154)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(106, 25)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Password"
        '
        'BtnGo
        '
        Me.BtnGo.Cursor = System.Windows.Forms.Cursors.Default
        Me.BtnGo.Location = New System.Drawing.Point(362, 190)
        Me.BtnGo.Margin = New System.Windows.Forms.Padding(4)
        Me.BtnGo.Name = "BtnGo"
        Me.BtnGo.Size = New System.Drawing.Size(182, 62)
        Me.BtnGo.TabIndex = 3
        Me.BtnGo.Text = "Login"
        Me.BtnGo.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ActiveBorder
        Me.Panel1.Controls.Add(Me.SettingsBtn)
        Me.Panel1.Controls.Add(Me.SkinBox)
        Me.Panel1.Controls.Add(Me.SubmitErrorBtn)
        Me.Panel1.Controls.Add(Me.Label8)
        Me.Panel1.Controls.Add(Me.Label7)
        Me.Panel1.Controls.Add(Me.DownloadProgress)
        Me.Panel1.Controls.Add(Me.Label6)
        Me.Panel1.Controls.Add(Me.RunBTN)
        Me.Panel1.Controls.Add(Me.InstallBTN)
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.PacksList)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.LogOutBtn)
        Me.Panel1.Controls.Add(Me.LoggedIn)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.SelectedUserTxt)
        Me.Panel1.Controls.Add(Me.BtnGo)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.PasswordTxt)
        Me.Panel1.Controls.Add(Me.UserNameTxt)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(4)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1496, 1140)
        Me.Panel1.TabIndex = 6
        '
        'SettingsBtn
        '
        Me.SettingsBtn.Location = New System.Drawing.Point(1080, 827)
        Me.SettingsBtn.Margin = New System.Windows.Forms.Padding(4)
        Me.SettingsBtn.Name = "SettingsBtn"
        Me.SettingsBtn.Size = New System.Drawing.Size(350, 60)
        Me.SettingsBtn.TabIndex = 22
        Me.SettingsBtn.Text = "Settings"
        Me.SettingsBtn.UseVisualStyleBackColor = True
        '
        'SkinBox
        '
        Me.SkinBox.AllowNavigation = False
        Me.SkinBox.AllowWebBrowserDrop = False
        Me.SkinBox.IsWebBrowserContextMenuEnabled = False
        Me.SkinBox.Location = New System.Drawing.Point(1112, 87)
        Me.SkinBox.Margin = New System.Windows.Forms.Padding(4)
        Me.SkinBox.MinimumSize = New System.Drawing.Size(26, 25)
        Me.SkinBox.Name = "SkinBox"
        Me.SkinBox.ScriptErrorsSuppressed = True
        Me.SkinBox.ScrollBarsEnabled = False
        Me.SkinBox.Size = New System.Drawing.Size(282, 600)
        Me.SkinBox.TabIndex = 21
        Me.SkinBox.Visible = False
        Me.SkinBox.WebBrowserShortcutsEnabled = False
        '
        'SubmitErrorBtn
        '
        Me.SubmitErrorBtn.Location = New System.Drawing.Point(1080, 760)
        Me.SubmitErrorBtn.Margin = New System.Windows.Forms.Padding(4)
        Me.SubmitErrorBtn.Name = "SubmitErrorBtn"
        Me.SubmitErrorBtn.Size = New System.Drawing.Size(350, 60)
        Me.SubmitErrorBtn.TabIndex = 20
        Me.SubmitErrorBtn.Text = "Report it here!"
        Me.SubmitErrorBtn.UseVisualStyleBackColor = True
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(926, 778)
        Me.Label8.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(145, 25)
        Me.Label8.TabIndex = 19
        Me.Label8.Text = "Found a bug?"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(78, 373)
        Me.Label7.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(171, 25)
        Me.Label7.TabIndex = 18
        Me.Label7.Text = "Download status"
        Me.Label7.Visible = False
        '
        'DownloadProgress
        '
        Me.DownloadProgress.Location = New System.Drawing.Point(362, 356)
        Me.DownloadProgress.Margin = New System.Windows.Forms.Padding(4)
        Me.DownloadProgress.Name = "DownloadProgress"
        Me.DownloadProgress.Size = New System.Drawing.Size(526, 38)
        Me.DownloadProgress.TabIndex = 17
        Me.DownloadProgress.Visible = False
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(116, 306)
        Me.Label6.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(134, 25)
        Me.Label6.TabIndex = 15
        Me.Label6.Text = "Ready to go!"
        Me.Label6.Visible = False
        '
        'RunBTN
        '
        Me.RunBTN.Location = New System.Drawing.Point(362, 290)
        Me.RunBTN.Margin = New System.Windows.Forms.Padding(4)
        Me.RunBTN.Name = "RunBTN"
        Me.RunBTN.Size = New System.Drawing.Size(216, 60)
        Me.RunBTN.TabIndex = 14
        Me.RunBTN.Text = "Run?"
        Me.RunBTN.UseVisualStyleBackColor = True
        Me.RunBTN.Visible = False
        '
        'InstallBTN
        '
        Me.InstallBTN.Location = New System.Drawing.Point(362, 223)
        Me.InstallBTN.Margin = New System.Windows.Forms.Padding(4)
        Me.InstallBTN.Name = "InstallBTN"
        Me.InstallBTN.Size = New System.Drawing.Size(216, 60)
        Me.InstallBTN.TabIndex = 13
        Me.InstallBTN.Text = "Install?"
        Me.InstallBTN.UseVisualStyleBackColor = True
        Me.InstallBTN.Visible = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(30, 240)
        Me.Label5.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(222, 25)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Modpack not installed"
        Me.Label5.Visible = False
        '
        'PacksList
        '
        Me.PacksList.FormattingEnabled = True
        Me.PacksList.Location = New System.Drawing.Point(362, 175)
        Me.PacksList.Margin = New System.Windows.Forms.Padding(4)
        Me.PacksList.Name = "PacksList"
        Me.PacksList.Size = New System.Drawing.Size(382, 33)
        Me.PacksList.TabIndex = 11
        Me.PacksList.Visible = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(174, 179)
        Me.Label4.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(77, 25)
        Me.Label4.TabIndex = 10
        Me.Label4.Text = "Packs:"
        Me.Label4.Visible = False
        '
        'LogOutBtn
        '
        Me.LogOutBtn.Location = New System.Drawing.Point(1080, 694)
        Me.LogOutBtn.Margin = New System.Windows.Forms.Padding(4)
        Me.LogOutBtn.Name = "LogOutBtn"
        Me.LogOutBtn.Size = New System.Drawing.Size(350, 60)
        Me.LogOutBtn.TabIndex = 9
        Me.LogOutBtn.Text = "Logout?"
        Me.LogOutBtn.UseVisualStyleBackColor = True
        Me.LogOutBtn.Visible = False
        '
        'LoggedIn
        '
        Me.LoggedIn.Enabled = False
        Me.LoggedIn.Location = New System.Drawing.Point(362, 127)
        Me.LoggedIn.Margin = New System.Windows.Forms.Padding(4)
        Me.LoggedIn.Name = "LoggedIn"
        Me.LoggedIn.ReadOnly = True
        Me.LoggedIn.Size = New System.Drawing.Size(210, 31)
        Me.LoggedIn.TabIndex = 8
        Me.LoggedIn.Text = "Currently Logged In!"
        Me.LoggedIn.Visible = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(998, 50)
        Me.Label3.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 25)
        Me.Label3.TabIndex = 7
        Me.Label3.Text = "Status"
        '
        'SelectedUserTxt
        '
        Me.SelectedUserTxt.Enabled = False
        Me.SelectedUserTxt.Location = New System.Drawing.Point(1080, 46)
        Me.SelectedUserTxt.Margin = New System.Windows.Forms.Padding(4)
        Me.SelectedUserTxt.Name = "SelectedUserTxt"
        Me.SelectedUserTxt.Size = New System.Drawing.Size(350, 31)
        Me.SelectedUserTxt.TabIndex = 6
        Me.SelectedUserTxt.Text = "No User Selected"
        '
        'Launch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(12.0!, 25.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1496, 912)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = false
        Me.MinimizeBox = false
        Me.Name = "Launch"
        Me.Text = "Wybar Minecraft Launcher"
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents UserNameTxt As TextBox
    Friend WithEvents PasswordTxt As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents BtnGo As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents SelectedUserTxt As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents LoggedIn As TextBox
    Friend WithEvents LogOutBtn As Button
    Friend WithEvents PacksList As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents InstallBTN As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents RunBTN As Button
    Friend WithEvents Label7 As Label
    Friend WithEvents DownloadProgress As ProgressBar
    Friend WithEvents SubmitErrorBtn As Button
    Friend WithEvents Label8 As Label
    Friend WithEvents SkinBox As WebBrowser
    Friend WithEvents SettingsBtn As Button
End Class
