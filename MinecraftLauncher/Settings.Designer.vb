﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Settings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.CancelBtn = New System.Windows.Forms.Button()
        Me.DoneBtn = New System.Windows.Forms.Button()
        Me.SuspendLayout
        '
        'CancelBtn
        '
        Me.CancelBtn.Location = New System.Drawing.Point(35, 301)
        Me.CancelBtn.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.CancelBtn.Name = "CancelBtn"
        Me.CancelBtn.Size = New System.Drawing.Size(57, 27)
        Me.CancelBtn.TabIndex = 0
        Me.CancelBtn.Text = "Cancel"
        Me.CancelBtn.UseVisualStyleBackColor = true
        '
        'DoneBtn
        '
        Me.DoneBtn.Location = New System.Drawing.Point(201, 301)
        Me.DoneBtn.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.DoneBtn.Name = "DoneBtn"
        Me.DoneBtn.Size = New System.Drawing.Size(57, 27)
        Me.DoneBtn.TabIndex = 1
        Me.DoneBtn.Text = "Done"
        Me.DoneBtn.UseVisualStyleBackColor = true
        '
        'Settings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(571, 391)
        Me.Controls.Add(Me.DoneBtn)
        Me.Controls.Add(Me.CancelBtn)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "Settings"
        Me.Text = "Settings"
        Me.ResumeLayout(false)

End Sub

    Friend WithEvents CancelBtn As Button
    Friend WithEvents DoneBtn As Button
End Class
