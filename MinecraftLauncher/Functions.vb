﻿'Copyright © 2016 - 2017, Luke Wybar and contributors. All Rights Reserved.

Public Class Json
    Public Value As String

    Public Overrides Function ToString() As String
        Return Value
    End Function
End Class

Public Class Pack
    Public ID As Integer
    Public Name As String
    Public Version As Decimal

    Public Overrides Function ToString() As String
        Return Name
    End Function

End Class

Public Class Data
    Public ID as Integer
    Public Data as String

    Public Overrides Function ToString() As String
        Return Data
    End Function
    
    Public Function WriteOut() As String
        Return(ID & "," & Data)
    End Function


End Class
Public Class Login
    Public AccessToken As String
    Public UserName As String
    Public UserID As String

    public Function GetToken() As String
        Return(accessToken)
    End Function
    public Function GetUserName() As String
        Return(UserName)
    End Function
    public Function GetUserID() As String
        Return(UserID)
    End Function
End Class
